const data = {
  type: 'object',
  properties: {},
  required: []
}

let processArray = [data.properties]
let processIndex = 0

const registered = {}

const stringifyProp = (name, prop, isRequired) => {
  const requiredColon = isRequired ? ":" : "?:";
  if(prop.type === 'string') {
    return `${name}${requiredColon} string`
  } else if(prop.type === 'array') {
    return `${name}${requiredColon} ${name}[]`
  } else {
    registered[name] = prop
    return `${name}${requiredColon} ${name}`
  }
}

const makeType = (obj) => {
  const topLevelProperties = Object.keys(obj);
  return topLevelProperties.map(objectName => {
    let str = "export interface " + objectName + " {\n"

    const objectBody = obj[objectName]
    const { type, properties, required } = objectBody;

    str += Object.entries(properties).map(([childPropName, childProp]) => {

      if(childProp.type === 'object') {
        if(!registered[childPropName]) {
          processArray.push({ [childPropName]: childProp })
        }
      } else if (childProp.type === 'array') {
        processArray.push({ [childPropName]: childProp.items[0] })
      }
      return '\t' + stringifyProp(
        childPropName,
        childProp,
        required.includes(childPropName)
      )
    }).join('\n')

    str+="\n}"
    return str;

  })
}

const types = []
while (processIndex < processArray.length) {
  const processed = makeType(processArray[processIndex])
  types.push(processed)
  processIndex++
}

console.log(types.join('\n\n'))

